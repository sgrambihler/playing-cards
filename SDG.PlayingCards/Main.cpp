// Playing Cards
// Sawyer Grambihler

#include <iostream>
#include <conio.h>

using namespace std;

enum class Suit {
	Diamonds,
	Hearts,
	Clubs,
	Spades
};

enum class Rank {
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

struct Card
{
	Suit Suit;
	Rank Rank;
};

int main() {

	Card c1;
	c1.Rank = Rank::King;
	c1.Suit = Suit::Spades;

	(void)_getch();
	return 0;
}